import React, {useEffect, useState} from 'react';
import {Grid} from "@react-css/grid";
import {Alert, AppBar, Box, Button, CircularProgress, Modal, Paper, Snackbar, Toolbar, Typography} from "@mui/material";

import _ from "lodash";

function generateArray(length: number) {
    return Array.from({length: length}, (v, k) => k + 1)
}

enum CharacterStatus {
    unknown,
    notPresent,
    present,
    correctPosition,
}


class CharacterGridItem {
    constructor(s: string, rowNum: number, colNum: number) {
        this.character = s;
        this.rowNum = rowNum;
        this.colNum = colNum;
        this.status = CharacterStatus.unknown;
    }

    public character: string;
    public rowNum: number;
    public colNum: number;
    public status: CharacterStatus;
}


function getRandomInt(max: number) {
    return Math.floor(Math.random() * max);
}


async function getRandomTag(): Promise<string> {
    const randomNum = getRandomInt(34) + 1;
    const url = `https://danbooru.donmai.us/tags.json?page=${randomNum}&search[category]=0&search[name_regex]=%5E[a-z]%7B5%7D%24&search[order]=count&search[post_count]=%3E20`
    const response = await fetch(url);
    const tags = await response.json();
    const index = getRandomInt(tags.length);
    return tags[index]["name"];
}


function App() {
    const [word, setWord] = useState<string>("");
    const [currentRow, setCurrentRow] = useState(0);
    const [currentColumn, setCurrentColumn] = useState(0);
    const [characterGrid, setCharacterGrid] = useState<Array<CharacterGridItem>>(
        generateArray(6).map(function (rowNum) {
            return generateArray(word.length).map(function (colNum) {
                return new CharacterGridItem("", rowNum, colNum);
            });
        }).flat()
    );
    const [showGameOverModal, setShowGameOverModal] = useState(false);
    const [showErrorSnackBar, setShowErrorSnackBar] = useState(false);

    const [gridItems, setGridItems] = useState<Array<JSX.Element>>(characterGrid.map(function (cgi) {
        return (
            <Grid.Item key={`grid_item__${cgi.rowNum}_${cgi.colNum}`} row={cgi.rowNum} column={cgi.colNum}>
                <Paper style={{width: "100%", height: "100%", textAlign: "center", alignItems: "center"}}>
                    <Typography
                        variant={"h1"}
                    >
                        {cgi.character}
                    </Typography>
                </Paper>
            </Grid.Item>
        )
    }));

    async function doesTagExist(_tag: string): Promise<boolean> {
        const verifyURL = `https://danbooru.donmai.us/tags.json?search[name_matches]=${_tag}`
        return await fetch(verifyURL).then(r => r.json()).then(function (json: Array<{post_count: number}>) {
            return json.length === 1 && Number(json[0].post_count) > 0
        });
    }

    function handleKeyUp(event: { key: string }) {
        if (currentColumn != word.length && event.key.length == 1) {
            setCharacterGrid((prevState) => {

                const newState = _.cloneDeep(prevState);
                newState[currentRow * word.length + currentColumn].character = event.key.toUpperCase();
                return newState;
            });
            setCurrentColumn(currentColumn + 1);
        } else if (event.key === "Backspace" && currentColumn !== 0) {
            setCharacterGrid((prevState) => {
                const newState = Array.from(prevState);
                newState[currentRow * word.length + (currentColumn - 1)].character = "";
                return newState;
            })
            setCurrentColumn((prevState) => {
                return prevState - 1
            });
        } else if (event.key === "Enter" && currentColumn === word.length) {
            let guess = "";
            for (let colIndex = 0; colIndex < word.length; colIndex++) {
                guess = guess + characterGrid[currentRow * word.length + colIndex].character;
            }
            doesTagExist(guess).then(result => {
                if (!result) {
                    setShowErrorSnackBar(true);
                    return
                }
                let isAllPosCorrect = true;

                for (let colIndex = 0; colIndex < word.length; colIndex++) {
                    const currentCharacter = characterGrid[currentRow * word.length + colIndex]
                    let newStatus: CharacterStatus
                    if (!word.includes(currentCharacter.character.toLowerCase())) {
                        isAllPosCorrect = false;
                        newStatus = CharacterStatus.notPresent;
                    } else if (word.charAt(colIndex) !== currentCharacter.character.toLowerCase()) {
                        isAllPosCorrect = false;
                        newStatus = CharacterStatus.present;
                    } else {
                        newStatus = CharacterStatus.correctPosition
                    }
                    setCharacterGrid((prevState) => {

                        const newState = _.cloneDeep(prevState);
                        newState[currentRow * word.length + colIndex].status = newStatus;
                        return newState;
                    });
                }
                if (currentRow === 5 || isAllPosCorrect) {
                    setShowGameOverModal(true);
                    return
                }
                setCurrentRow(currentRow + 1);
                setCurrentColumn(0);
            })
        }
    }


    let colorMap: { [key in CharacterStatus]?: string }
    colorMap = {}
    colorMap[CharacterStatus.unknown] = "black"
    colorMap[CharacterStatus.notPresent] = "red"
    colorMap[CharacterStatus.present] = "orange"
    colorMap[CharacterStatus.correctPosition] = "green"

    useEffect(() => {
        setGridItems(() => {

            return characterGrid.map(function (cgi) {
                return (
                    <Grid.Item key={`grid_item__${cgi.rowNum}_${cgi.colNum}`} row={cgi.rowNum} column={cgi.colNum}>
                        <Paper style={{width: "100%", height: "100%", textAlign: "center", alignItems: "center"}}>
                            <Typography
                                style={{color: colorMap[cgi.status], fontSize: "6vh"}}

                            >
                                {cgi.character}
                            </Typography>
                        </Paper>
                    </Grid.Item>
                )
            })
        });
    }, [characterGrid]);

    async function _setWord() {
        const _word = await getRandomTag();
        console.log(_word);
        setWord(_word);
    }

    useEffect(() => {
        void _setWord();
    }, []);

    useEffect(() => {
        setCharacterGrid(
            generateArray(6).map(function (rowNum) {
                return generateArray(word.length).map(function (colNum) {
                    return new CharacterGridItem("", rowNum, colNum);
                });
            }).flat()
        )
    }, [word])

    function resetGame() {
        void _setWord();

        setCurrentColumn(0);
        setCurrentRow(0);
    }

    return (
        <div>
            <AppBar position="static" style={{marginBottom: "10px"}}>
                <Toolbar>
                    <Typography style={{fontSize: "6vh"}}>Danboodle</Typography>
                </Toolbar>
            </AppBar>
            <Modal open={showGameOverModal}>
                <Box style={{
                    width: "50vw",
                    top: "50%",
                    left: "50%",
                    transform: 'translate(-50%, -50%)',
                    height: "50vh",
                    position: "absolute"
                }}>
                    <Paper style={{padding: "5px"}}>
                        <Grid rows={"10vh 35vh 5vh"}>
                            <Grid.Item row={1}>
                                <Typography style={{fontSize: "8vh"}}>Game Over!</Typography>
                            </Grid.Item>
                            <Grid.Item row={2} justifySelfCenter alignSelfCenter>
                                <p>
                                    <span><Typography>The word was</Typography></span>
                                    <span><Typography style={{fontWeight: "bold", fontSize: "150%"}}>{word}</Typography></span>
                                </p>
                            </Grid.Item>
                            <Grid.Item row={3} alignSelfEnd>
                                <Button variant={"contained"} fullWidth onClick={() => {
                                    resetGame();
                                    setShowGameOverModal(false);
                                }}>Replay</Button>
                            </Grid.Item>
                        </Grid>
                    </Paper>
                </Box>
            </Modal>
            <Snackbar
                open={showErrorSnackBar}
                autoHideDuration={4000}
                onClose={(event, reason) => {
                    if (reason === 'clickaway') {
                        return;
                    }

                    setShowErrorSnackBar(false);
                }}
                anchorOrigin={{vertical: "bottom", horizontal: "right"}}
            >
                <Alert
                    // @ts-ignore
                    onClose={(
                        event: unknown,
                        reason: string
                    ) => {

                        if (reason === 'clickaway') {
                            return;
                        }

                        setShowErrorSnackBar(false);
                    }}
                    severity="error"
                    sx={{width: '100%'}}
                >Your guess is not a valid tag</Alert>
            </Snackbar>
            {word !== "" ? (
                <div
                    className="App"
                    onKeyUp={handleKeyUp}
                    tabIndex={-1}
                    style={{outline: "none"}}
                >
                    <Grid
                        rows={"10vh 10vh 10vh 10vh 10vh 10vh"}
                        columns={generateArray(word.length).map(() => {
                            return "10vh"
                        }).join(" ")}
                        columnGap={"10px"}
                        rowGap={"25px"}
                    >{gridItems}
                    </Grid>
                </div>
            ) : <CircularProgress/>
            }
        </div>
    )
}

export default App;
